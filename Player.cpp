#include "Player.h"
#include <iostream>
using namespace std;




void player_init(Player* player){
    player->money = 10000;
    player->unit_count = 0;
    player->units = NULL; //on met l'adresse de units à 0, aucune mémoire allouée
}



Unit* add_unit(int type, Player* player){
    player->unit_count++;
    player->units = (Unit*) realloc(player->units, player->unit_count * sizeof(Unit));

    Unit* last_unit = &player->units[player->unit_count - 1]; //player->unit + unit_count (on fonctionne en adresse)

    switch (type)
    {

    case 2:
        soldat_init(last_unit, player);
        break;

    case 4:
        bazooka_init(last_unit, player);
        break;

    case 0:
        tank_init(last_unit, player);
        break;

    }

    //last_unit->owner = player;
    return last_unit;
}

void remove_unit(Player* player){
    player->unit_count--;
}

