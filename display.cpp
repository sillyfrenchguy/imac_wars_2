#include "Game.h"
#include "Map.h"
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"

#include "SDL_image.h"
#include <iostream>
using namespace std;



extern bool isInMenu, isInShop;
//extern const int UNITS_PRICE[NB_UNIT] = {1000, 2000, 3000}; // Revoir la structure des unit, sinon pb d'inclusion de trop de fichiers partout


TTF_Font *font, *fontTitres;
int count = 0; // nb d'image

Uint32 frameStart;
int frameTime;

char* map_environnement[] = {
"000000089000000",
"000000067000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000",
"000000000000000"};

SDL_Color blanc = {255,255,255,255};
SDL_Color noir = {0,0,0,255};


SDL_Surface* screen,*tileset, *tilesetunit, *interface, *map, *mapUnit, *fond, *screenTemp;
SDL_Surface* moneyText1, *moneyText2, *unitInfo1TextHP, *unitInfo1TextType, *unitInfo2TextHP, *unitInfo2TextType, *menuBg, *logo, *startButton, *startText;
SDL_Surface* shopSurface, *shopButton, *shopUnit0, *shopUnit1, *shopUnit2, *shopUnit0Sprite, *shopUnit1Sprite, *shopUnit2Sprite, *shopUnit0Text, *shopUnit1Text, *shopUnit2Text;


char moneyChar1[32];
char moneyChar2[32];

SDL_Rect rectDestMoney1 = {margeleft, margetop, 1, 1};
SDL_Rect rectDestMoney2 = {margeright, margetop, 1, 1};
SDL_Rect rectUnitInfo1 = {margeleft, 0, 1, 1};
SDL_Rect rectUnitInfo2 = {margeright, 0, 1, 1};
SDL_Rect zoneMap = {(SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2,0,MAP_WIDTH*LARGEUR_TILE,MAP_HEIGHT*HAUTEUR_TILE};
SDL_Rect unitRect ={0,0,LARGEUR_TILE,HAUTEUR_TILE};

SDL_Rect screenRect = {0,0,SCREEN_WIDTH,SCREEN_HEIGHT};
SDL_Rect zoneJ1 = {0,0,(SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2,SCREEN_HEIGHT};
SDL_Rect zoneJ2 = {(SCREEN_WIDTH+MAP_WIDTH*LARGEUR_TILE)/2,0,(SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2,SCREEN_HEIGHT};
SDL_Rect zoneStart, zoneLogo;
SDL_Rect shopUnit0Rect = {margeleft,MAP_HEIGHT*HAUTEUR_TILE/2,LARGEUR_TILE,HAUTEUR_TILE};
SDL_Rect shopUnit1Rect = {(MAP_WIDTH*LARGEUR_TILE-LARGEUR_TILE)/2,MAP_HEIGHT*HAUTEUR_TILE/2,LARGEUR_TILE,HAUTEUR_TILE};
SDL_Rect shopUnit2Rect = {margeright,MAP_HEIGHT*HAUTEUR_TILE/2,LARGEUR_TILE,HAUTEUR_TILE}; 

Game game;



void display_info(Game* game);
void display_menu();

void shopInit(){
		shopUnit0Sprite = SDL_CreateRGBSurface(0,1,1,32,0,0,0,0);
		shopUnit1Sprite = SDL_CreateRGBSurface(0,1,1,32,0,0,0,0);
		shopUnit2Sprite = SDL_CreateRGBSurface(0,1,1,32,0,0,0,0);

		shopUnit0 = SDL_CreateRGBSurface(0,1,1,32,0,0,0,0);
		shopUnit1 = SDL_CreateRGBSurface(0,1,1,32,0,0,0,0);
		shopUnit2 = SDL_CreateRGBSurface(0,1,1,32,0,0,0,0);

		shopSurface = IMG_Load("shopBg.png");

		shopUnit0Sprite->w = LARGEUR_TILE;
		shopUnit0Sprite->h = HAUTEUR_TILE;
		shopUnit1Sprite->w = LARGEUR_TILE;
		shopUnit1Sprite->h = HAUTEUR_TILE;
		shopUnit2Sprite->w = LARGEUR_TILE;
		shopUnit2Sprite->h = HAUTEUR_TILE;

		SDL_BlitSurface(tilesetunit, &unitRect, shopUnit0Sprite,NULL);
		unitRect.x += LARGEUR_TILE;
		SDL_BlitSurface(tilesetunit, &unitRect, shopUnit1Sprite, NULL);
		unitRect.x += LARGEUR_TILE;
		SDL_BlitSurface(tilesetunit, &unitRect, shopUnit2Sprite, NULL);

		shopUnit0Text = TTF_RenderText_Blended(font,"1000",blanc);
		shopUnit1Text = TTF_RenderText_Blended(font,"2000",blanc);
		shopUnit2Text = TTF_RenderText_Blended(font,"3000",blanc);

		

		shopUnit0->h += shopUnit0Text->h + 10;
		shopUnit1->h += shopUnit0Text->h + 10;
		shopUnit1->h += shopUnit0Text->h + 10;
		shopUnit0->w += shopUnit0Text->w + 10;
		shopUnit1->w += shopUnit0Text->w + 10;
		shopUnit1->w += shopUnit0Text->w + 10;

		SDL_Rect shopSpriteUnit = {(shopUnit0->w - shopUnit0Sprite->w)/2,0,LARGEUR_TILE,HAUTEUR_TILE};
		SDL_Rect shopTextUnit = {10,HAUTEUR_TILE,shopUnit0Text->w,shopUnit0Text->h};

		SDL_BlitSurface(shopUnit0Sprite, NULL, shopUnit0, &shopSpriteUnit);
		SDL_BlitSurface(shopUnit1Sprite, NULL, shopUnit1, &shopSpriteUnit);
		SDL_BlitSurface(shopUnit2Sprite, NULL, shopUnit2, &shopSpriteUnit);
		SDL_BlitSurface(shopUnit0Text,NULL,shopUnit0, &shopTextUnit);
		SDL_BlitSurface(shopUnit1Text,NULL,shopUnit1, &shopTextUnit);
		SDL_BlitSurface(shopUnit2Text,NULL,shopUnit2, &shopTextUnit);
		
		shopUnit0Rect.w = shopUnit0->w;
		shopUnit0Rect.h = shopUnit0->h;
		shopUnit1Rect.w = shopUnit1->w;
		shopUnit1Rect.h = shopUnit1->h;
		shopUnit2Rect.w = shopUnit2->w;
		shopUnit2Rect.h = shopUnit2->h;


		SDL_BlitSurface(shopUnit0,NULL,shopSurface,&shopUnit0Rect);
		SDL_BlitSurface(shopUnit1,NULL,shopSurface,&shopUnit1Rect);
		SDL_BlitSurface(shopUnit2,NULL,shopSurface,&shopUnit2Rect);

		isInShop = false;

}

void display_init(){
	if(SDL_Init(SDL_INIT_EVERYTHING) == 0){

		TTF_Init();
		IMG_Init(IMG_INIT_PNG);

		
		fond = SDL_CreateRGBSurface(0,SCREEN_WIDTH,SCREEN_HEIGHT,32,0,0,0,255);
		map = SDL_CreateRGBSurface(0,MAP_WIDTH*LARGEUR_TILE,MAP_HEIGHT*HAUTEUR_TILE,32,0,0,0,0);
		mapUnit = SDL_CreateRGBSurface(0,MAP_WIDTH*LARGEUR_TILE,MAP_HEIGHT*HAUTEUR_TILE,32,0,0,0,0);
		screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_RESIZABLE | SDL_DOUBLEBUF | SDL_HWSURFACE);
		screenTemp = SDL_CreateRGBSurface(0,SCREEN_WIDTH,SCREEN_HEIGHT,32,0,0,0,0);
		

		menuBg = IMG_Load("menuBg.png");
		tileset = IMG_Load("tile_map2.png");
		tilesetunit = IMG_Load("map_unit.png");


		SDL_WM_SetCaption("IMACWARS 2", NULL);

		font = TTF_OpenFontIndex("arial.ttf",SCREEN_HEIGHT/40,0);
		fontTitres = TTF_OpenFontIndex("arial.ttf",SCREEN_HEIGHT/20,0);

		logo = TTF_RenderText_Blended(fontTitres,"IMACWARS 2", blanc);
		startText = TTF_RenderText_Blended(fontTitres,"START", noir);


		startButton = SDL_CreateRGBSurface(0,startText->w,startText->h,32,0,0,0,0);
		SDL_FillRect(startButton,NULL,SDL_MapRGB(startButton->format,150,230,0));

		SDL_BlitSurface(startText,NULL,startButton,NULL);

		zoneLogo.x = (SCREEN_WIDTH - logo->w)/2;
		zoneLogo.y = margetop;
		zoneLogo.w = logo->w;
		zoneLogo.h = logo->h;
		zoneStart.x = (SCREEN_WIDTH - startButton->w)/2;
		zoneStart.y = 2*SCREEN_HEIGHT/3;
		zoneStart.w = startButton->w;
		zoneStart.h = startButton->h;

		shopInit();

		if (map == NULL){
			cout << "Pb surface" << endl;
		}


        if (!tileset)
        {
            printf("Echec de chargement tile_map2.png\n");
            SDL_Quit();
            system("pause");
            exit(-1);
        }

        if (!tilesetunit)
        {
            printf("Echec de chargement map_unit.png\n");
            SDL_Quit();
            system("pause");
            exit(-1);
        }
		//debug

		if (screen){
			cout << "Initialised..!" << endl;
		}


		if (TTF_Init()){
			cout << "TTF Initialised"<< endl;
		}

		game_init(&game);


        Afficher_map(map,tileset,map_environnement,MAP_WIDTH,MAP_HEIGHT);
        cout << "coucou" << endl;

        //Afficher_unit(screen,tileset,&game->map,MAP_WIDTH,MAP_HEIGHT);
	}

	else{
		cout << "Erreur d'initialisation de la SDL : ";
		cerr << SDL_GetError() << endl;
		SDL_Quit();
	}

}

void display_update(){
	SDL_UpdateRect(screen, 0, 0, 0, 0);
	SDL_Flip(screen);
	frameStart = SDL_GetTicks();

	if (not isInMenu){
		display(&game);
	}
	else {
		display_menu();
	}

	//cout << count << endl;
	frameTime = SDL_GetTicks() - frameStart;
	gameHandleEvent(&game);
	if (frameDelay > frameTime){
		SDL_Delay(frameDelay - frameTime);
	}
	count++;

	//SDL_FreeSurface(screen);
}

void display(Game* game){
		//blit la map et les unit + l'UI ou les menus,
		//dépend du contenu des autres fichiers
		//SDL_BlitSurface(renderer,NULL,screen,NULL);

		SDL_BlitSurface(fond,NULL,screenTemp,NULL);
		//SDL_BlitSurface(fond,&zoneJ2,screenTemp,&zoneJ2);

        Afficher_unit(map,tilesetunit,&game->map,MAP_WIDTH,MAP_HEIGHT);
        
        SDL_BlitSurface(map,NULL,screenTemp,&zoneMap);
        display_info(game);

        // if (isInShop){
        // 	SDL_BlitSurface(shopSurface, NULL,screenTemp,&zoneMap);
        // }

        SDL_BlitSurface(screenTemp,NULL,screen,NULL);
	}

void display_menu(){
	SDL_BlitSurface(menuBg,NULL,screenTemp,&screenRect);
	SDL_BlitSurface(logo,NULL,screenTemp,&zoneLogo);
	SDL_BlitSurface(startButton,NULL,screenTemp,&zoneStart);
	SDL_BlitSurface(screenTemp,NULL,screen,NULL);
}

void display_info(Game* game){
    sprintf(moneyChar1, "PLAYER1 : %d$",game->players[0].money);
    sprintf(moneyChar2, "PLAYER2 : %d$",game->players[1].money);
    char hp[10];
    char unitType[10];
    char unitInfo1Type[64] = {'\0'};
    char unitInfo1HP[64] = {'\0'};
	char unitInfo2Type[64]= {'\0'};
	char unitInfo2HP[64]= {'\0'};
	rectUnitInfo1.y = 2*margetop + rectDestMoney1.h;
	rectUnitInfo2.y = 2*margetop + rectDestMoney2.h;
	//cout << "Avant que ça crash?" << endl;

    for (int i =0; i<game->players[0].unit_count; i++){ // Boucle affichage info unit joueur 1
        sprintf(unitInfo1Type,"Type : ");
        sprintf(unitType, "%i", game->players[0].units[i].super);
        strcat(unitInfo1Type,unitType);
        sprintf(unitInfo1HP,"HP : ");
        sprintf(hp, "%i",(int)game->players[0].units[i].pv );
        strcat(unitInfo1HP,hp);
        unitInfo1TextType = TTF_RenderUTF8_Blended(font,unitInfo1Type, blanc);
        unitInfo1TextHP = TTF_RenderUTF8_Blended(font,unitInfo1HP, blanc);
        rectUnitInfo1.w = unitInfo1TextType->w;
        rectUnitInfo1.h = unitInfo1TextType->h;
        SDL_BlitSurface(unitInfo1TextType,NULL,screenTemp,&rectUnitInfo1);
        rectUnitInfo1.y += margetop;
        SDL_BlitSurface(unitInfo1TextHP,NULL,screenTemp,&rectUnitInfo1);
        rectUnitInfo1.y += margetop + unitInfo1TextHP->h ;
   	}

    for (int i =0; i<game->players[1].unit_count; i++){ // Boucle affichage info unit joueur 2
        sprintf(unitInfo2Type,"Type : ");
        sprintf(unitType, "%i", game->players[1].units[i].super);
        strcat(unitInfo2Type,unitType);
        sprintf(unitInfo2HP,"HP : ");
        sprintf(hp, "%i",(int)game->players[1].units[i].pv );
        strcat(unitInfo2HP,hp);
        unitInfo2TextType = TTF_RenderUTF8_Blended(font,unitInfo2Type, blanc);
        unitInfo2TextHP = TTF_RenderUTF8_Blended(font,unitInfo2HP, blanc);
        rectUnitInfo2.w = unitInfo2TextType->w;
        rectUnitInfo2.h = unitInfo2TextType->h;
        SDL_BlitSurface(unitInfo1TextType,NULL,screenTemp,&rectUnitInfo2);
        rectUnitInfo2.y += margetop;
        SDL_BlitSurface(unitInfo2TextHP,NULL,screenTemp,&rectUnitInfo2);
        rectUnitInfo2.y += margetop + unitInfo2TextHP->h;
    }

    if (!font){
        cout << "Police non initialisée :" << TTF_GetError()<< endl;
    }

    moneyText1 = TTF_RenderUTF8_Blended(font,moneyChar1, blanc);
    moneyText2 = TTF_RenderUTF8_Blended(font,moneyChar2, blanc);

    rectDestMoney1.w = moneyText1->w;
    rectDestMoney1.h = moneyText1->h;
    rectDestMoney2.w = moneyText2->w;
    rectDestMoney2.h = moneyText2->h;

    SDL_BlitSurface(moneyText1,NULL,screenTemp,&rectDestMoney1);
    SDL_BlitSurface(moneyText2,NULL,screenTemp,&rectDestMoney2);

   	SDL_FreeSurface(moneyText1);
   	SDL_FreeSurface(moneyText2);
 	SDL_FreeSurface(unitInfo1TextHP);
 	SDL_FreeSurface(unitInfo1TextType);
    SDL_FreeSurface(unitInfo2TextHP);
    SDL_FreeSurface(unitInfo2TextType);
}
// int main(){ // test;
// 	display_init();
// 	while (SDL_GetTicks() < 10000){
// 		display_update();
// 	}
// 	SDL_Quit();

// }

