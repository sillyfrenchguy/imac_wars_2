#include "Game.h"
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "SDL_image.h"
#include <iostream>
using namespace std;

bool isRunning = false;
bool isInMenu = false;
bool isInShop = false;

extern TTF_Font *font;
extern SDL_Surface *screen;


// extern const int SCREEN_WIDTH = 800;
// extern const int SCREEN_HEIGHT = 600;
extern SDL_Rect zoneMap, zoneStart; // Les boutons cliquables ! 
int currentPlayer = 0;


void game_init(Game* game) {
    //initialisation du jeu
    isRunning = true;
    map_init(&game->map);
    player_init(&game->players[0]);
    player_init(&game->players[1]);
    game->players[0].playerNumber = 0;
    game->players[1].playerNumber = 1;
    game->players[0].unit_count= 0;
    game->players[1].unit_count= 0;
    isInMenu = true;
}

void game_start(Game* game) {
    cout <<endl;
    cout << "                                CHOIX DES JOUEURS                               " <<endl;
    cout << "--------------------------------------------------------------------------------" <<endl;
    //1.choix des noms des joueurs
    for (int i = 0; i<NB_PLAYERS; i++){
        cout << endl ;
        cout << "JOUEUR " << (i+1) << ", saisis ton nom : " << endl;
        cin >> game->players[i].name;

    }
    cout << endl;
    cout << endl;
    cout << endl;
    cout << "                                  REGLES DU JEU                                " <<endl;
    cout << "-------------------------------------------------------------------------------" <<endl;
    cout << "                  Bonjour à vous chers ingénieurs créatifs !                   " << endl;
    cout << "        Préparez-vous à un combat exceptionnel, explosif et sanglant           " << endl;
    cout << "                     entre ♦ " << game->players[0].name << " et ♠ " << game->players[1].name << " !" << endl ;
    cout << endl ;
    cout << endl;
    cout << "Vous allez construire vos armées en achetant des unités." << endl;
    cout << "Vous avez le choix entre :" << endl;
    cout << "- " << UNITS_NAME[0] << " (" << UNITS_PRICE[0] << " $) : [Dextérité] 6 cases | [Force] 0.2  | [Portée]  1 case" << endl;
    cout << "- " << UNITS_NAME[1] << " (" << UNITS_PRICE[1] << " $) : [Dextérité] 3 cases | [Force] 0.35  | [Portée]  3 cases" << endl;
    cout << "- " << UNITS_NAME[2] << " (" << UNITS_PRICE[2] << " $) : [Dextérité] 2 cases | [Force] 0.5  | [Portée]  6 cases" << endl;
    cout << endl;
    cout << "Le plateau est une grille 2D numérotée de 0 à 9. " << endl;
    cout << "Tous vos déplacements et choix d’unités devront se faire grâce aux" << endl;
    cout << "coordonnées X (colonne) et Y (ligne) séparés par un caractère." << endl ;
    cout << endl;
    cout << "Lors du combat vous avez deux possibilités :"<<endl;
    cout << "- DÉPLACEMENT (vous déplacez votre unité selon la dextérité)"<<endl;
    cout << "- ATTAQUE (vous choisissez une unité adverse selon la portée de votre unité attaquante)" <<endl;
    cout << endl;
    cout << endl;
    cout << "                 Qui sera le dernier à avoir des unités en guerre ?             " << endl;
    cout << endl;
    cout << endl;
    cout << endl;

    cout << "                                DEBUT DU COMBAT                                 " <<endl;
    cout << "--------------------------------------------------------------------------------" <<endl;


    //2.choix des types d'unités et placements
    //boucle des joueurs
    for (int i = 0; i<NB_PLAYERS; i++){
        cout << game->players[i].name << ", tu as " << game->players[i].money << " $, tu peux acheter des unités :"<<endl;
        int type = 0;

        while (game->players[i].money > 0) {
            //boucle des unités
            for (int u = 0; u < NB_UNIT ; u++){
                cout << u+1 << " : " << UNITS_NAME[u] << " (" << UNITS_PRICE[u] << " $)" << endl;
            }

            //choix des types d'unités
            cout << "Je choisis l'unité : ";
            cin >> type;
            cout << endl;

            if (type >= 1 && type <= NB_UNIT && game->players[i].money - UNITS_PRICE[type - 1] >= 0 ){
                Unit* last_unit = add_unit(type, &game->players[i]);

                game->players[i].money -= last_unit->cost ;
                last_unit->owner = &game->players[i];


                //ajout dans les cellules de la map
                int x;
                int y;
                char c;
                cout << "Où souhaites-tu placer ton " << UNITS_NAME[type-1] << " dans le tableau ? (x;y)" << endl;
                cin >> x >> c >> y ;
                cout << endl;
                int placement = 0; //Feux rouge de placement
                while (placement == 0){ // Eviter de ce placer dans une place déjà prise
                    if ((game->map).cells[y][x] == NULL){
                        add_unit_map(last_unit, &game->map , x, y);
                        if (game->players[i].money != 0){
                            cout << "Il te reste " << game->players[i].money << " $"<<endl;
                        }
                        else {
                            cout << endl;
                            cout << "Tu n'as plus d'argent ! Ton armée est prête !"<< endl;
                            cout << endl;
                        }
                        placement = 1 ;
                    }
                    else {
                        cout << "Cette place est déjà prise...Saisis un autre emplacement :" << endl;
                        cin >> x >> c >> y ;
                    }
                }
            }
            else if (type == 0) {
                break;
            }
            else {
                cout << game->players[i].name << ", recommence, tu n'as pas assez d'argent ou tu as fais une erreur de frappe !"<< endl;
            }
        }

    }
}

void game_start2(Game* game) {
    //2.choix des types d'unités et placements
    //boucle des joueurs

    while (game->players[0].money > 0) {
        //choix des types d'unités

        //ajout dans les cellules de la map
        int px;
        int py;
        char c;
        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e)){
            switch(e.type){
                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    switch(e.button.button){
                        case SDL_BUTTON_LEFT:
                            printf("clic en (%d, %d)\n", e.button.x, e.button.y);
                            px = e.button.x;
                            px = px/LARGEUR_TILE;
                            px = px*(MAP_WIDTH/MAP_HEIGHT);

                            py = e.button.y;
                            py = py/HAUTEUR_TILE;
                            py = py*(MAP_WIDTH/MAP_HEIGHT);
                            printf("Test"); // ?

                            Unit* last_unit = add_unit(2, &game->players[0]);

                            last_unit->cost = 2000;

                            printf("Test");
                            cout << last_unit->cost << endl;
                            game->players[0].money -= last_unit->cost ;
                            //last_unit->owner = &game->players[0];
                            add_unit_map(last_unit, &game->map , px, py);

                            //Afficher_unit(screen,tileset,&game->map,MAP_WIDTH,MAP_HEIGHT);
                        }

                    default:
                        break;
            }
        }
    }
}

void game_duel(Game* game) {

    //boucle des joueurs
    cout << endl;
    map_draw(&game->map, game);
    cout << "[RECAP] Le combat se compose de : "<<endl;
    info_unit_map(&game->map);

    int survivant = 1;

    while(survivant == 1){
        int i = 0;
        while (i<NB_PLAYERS && survivant == 1){
            cout << endl;
            cout << game->players[i].name << ", c'est à ton tour, tu as " << game->players[i].unit_count << " unité, choisis-en une :"<<endl;

            //choix de l'unité et vérification du owner
            int x;
            int y;
            char c;
            cout << "(x;y)" ;
            cin >> x >> c >> y ;

            int possession = 0;
            while (possession == 0 ){
                if ((game->map.cells[y][x]) != NULL){ //gérer la selection d'une case vide
                    if ((*game->map.cells[y][x]).owner == &game->players[i]) { //eviter ne prendre une case qui ne nous appartient pas
                        possession = 1;
                    }

                    else {
                        cout << "Attention, cette unité ne t'appartient pas !" << endl;
                        cin >> x >> c >> y ;
                    }

                }
                else {
                    cout << "Il n'y a rien ici ..." <<endl;
                    cin >> x >> c >> y ;
                }
            }
            //choix de l'action
            cout << endl;
            cout << "Que souhaites-tu faire avec cette unité ?" << endl;
            for (int u = 0; u < NB_ACTION ; u++){
                cout << u+1 << " : " << UNITS_ACTION[u] << endl;
            }
            int type;
            cin >> type;

            Unit* unit_select = game->map.cells[y][x] ;


            if (type == 1){
                cout << "Où souhaites-tu déplacer ton unité ? (Elle a une dextérité de "  <<  (*game->map.cells[y][x]).move  << " case(s)) " << endl;
                int X;
                int Y;
                char C;
                cout << "(x;y)";
                cin >> X >> C >> Y ;
                cout <<endl;
                int distance = abs(X-x)+abs(Y-y) ;

                if (distance >0 && distance <=(*game->map.cells[y][x]).move && (game->map).cells[Y][X] == NULL ) {
                    cout << "Tu as bougé de " << distance << " cases." <<endl;
                    move_unit_map(unit_select, &game->map, x, y, X, Y);
                    map_draw(&game->map, game);
                    cout << endl;
                    cout << "RECAPITULATIF"<<endl;
                    info_unit_map(&game->map);

                }

                else {
                    cout << "Tu ne peux pas te déplacer ainsi... Passe-ton tour !" << endl;

                }
            }

            if (type == 2){
                cout << "Ton unité a une portée de "  <<  (*game->map.cells[y][x]).range  << " case(s). " << endl;
                cout << "Quelle unité veux-tu attaquer ?" << endl;
                int X;
                int Y;
                char C;
                cout << "(x;y)";
                cin >> X >> C >> Y ;
                if ((game->map.cells[Y][X]) != NULL){ //gérer la selection d'une case vide
                    int possession2 = 0; //verification de la cible pour éviter de se tuer soi-même
                    while (possession2 == 0 ){
                        if ((*game->map.cells[Y][X]).owner == &game->players[i]) {
                            cout << "Attention, cette unité est à toi !" <<endl;
                            cin >> x >> c >> y ;
                        }
                        else {
                            possession2 = 1;
                        }
                    }
                    int distance = abs(X-x)+abs(Y-y) ;

                    if (distance <= (*game->map.cells[y][x]).range) {
                        (*game->map.cells[Y][X]).pv -= (*game->map.cells[y][x]).power * (*game->map.cells[y][x]).pv ;
                        (*game->map.cells[y][x]).pv -= 0.2*(*game->map.cells[Y][X]).power * (*game->map.cells[Y][X]).pv ;

                        if((*game->map.cells[Y][X]).pv<=0){
                            cout << game->players[i].name << ", vous avez abattu l'unité placée aux coordonnées (" << X << ";" << Y << ")" << endl;
                            if(i==0){
                                remove_unit(&game->players[1]);

                            }
                            if(i==1){
                                remove_unit(&game->players[0]);

                            }
                            remove_unit_map(&game->map, X, Y);
                        }
                        else{
                            cout << game->players[i].name << ", vous avez fait perdre " << (*game->map.cells[y][x]).power  << " pv à l'unité placée aux coordonnées (" << X << ";" << Y << ")" << endl;
                        }
                        cout << endl;
                        map_draw(&game->map, game);
                        cout << "RECAPITULATIF"<<endl;
                        info_unit_map(&game->map);
                        cout << endl;

                        if(game->players[1].unit_count ==0 || game->players[0].unit_count ==0 ){
                            survivant = 0;
                        }
                    }

                    else {
                        cout << "Tu ne peux pas l'attaquer... Passe-ton tour !" << endl;

                    }
                }

                else {
                    cout << "Pas de chance, tu as loupé ta cible... Passe ton tour !" <<endl;

                }

            }
        i++;
        }
    }
    if(game->players[0].unit_count ==0){
        cout << endl;
        cout << "                                FIN DU COMBAT                                   " <<endl;
        cout << "--------------------------------------------------------------------------------" <<endl;
        cout << game->players[0].name << ", toutes tes unités ont été détruites ! Tu as perdu la partie." << endl;
        cout << game->players[1].name << ", tu as remporté cette bataille mais pas la guerre ! ;)" << endl;
    }
    if(game->players[1].unit_count ==0){
        cout << endl;
        cout << "                                FIN DU COMBAT                                 " <<endl;
        cout << "--------------------------------------------------------------------------------" <<endl;
        cout << game->players[1].name << ", toutes tes unités ont été détruites ! Tu as perdu la partie." << endl;
        cout << game->players[0].name << ", tu as remporté cette bataille mais pas la guerre ! ;)" << endl;
    }
}

void gameClean(){
    cout << "Game quitting..." << endl;
    TTF_CloseFont(font);
    TTF_Quit();
    IMG_Quit();
    SDL_FreeSurface(screen);
    SDL_Quit();
    cout << "Game quit" << endl;
}

bool isOnButton(SDL_Event e, SDL_Rect rect){
    if(e.button.x>rect.x && e.button.x < rect.x + rect.w && e.button.y >rect.y && e.button.y < rect.y + rect.h){
        return true;
    }
    else {
        return false;
    }
}

void gameHandleEvent(Game* game){
    int px=0;
    int py=0;
    SDL_Event event;
    SDL_PollEvent(&event);

    // le 0 correspond au joueur 1 et le 1 correspond au joueur 2; il suffit de modifier cette variable pour avoir la bonne couleur d'unit
    int type = 0; // de la même manière : Tank = 0 ; Soldat = 2; Bazooka = 4

    switch (event.type){
        case SDL_QUIT :
            isRunning = false;
            gameClean();
            break;

        case SDL_MOUSEBUTTONDOWN :
            cout << "Clique en : " << event.button.x << " ; " << event.button.y << endl;

            if (isInMenu){
                if (isOnButton(event,zoneStart)){
                    isInMenu = false;
                    cout << "Quitting menu"<<endl;
                    isInShop = true;
                }


            }

            else{
                if ((zoneMap.x + zoneMap.w > event.button.x ) && (event.button.x > zoneMap.x)&&(zoneMap.y + zoneMap.h>event.button.y)&&(event.button.y>zoneMap.y)){ // Si le clique est sur la carte
                    px = event.button.x - (SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2;
                    px = px/LARGEUR_TILE;
                    px = px*(MAP_WIDTH/MAP_HEIGHT);

                    py = event.button.y;
                    py = py/HAUTEUR_TILE;
                    py = py*(MAP_WIDTH/MAP_HEIGHT);

                    Unit* last_unit = add_unit(type, &game->players[currentPlayer]);

                    game->players[currentPlayer].money -= last_unit->cost ;
                    cout << "Argent restant: " << game->players[currentPlayer].money << endl;
                    
                    cout << "px : " << px << endl;
                    cout << "py : " << py << endl;
                    add_unit_map(last_unit, &game->map , px, py);
                    currentPlayer = (currentPlayer + 1 )%2;
                }
                cout << currentPlayer<<endl;
                cout << "unit j1 "<< game->players[0].unit_count<< endl;
                cout << "unit j2 "<< game->players[1].unit_count<< endl;

            }



            break;


    }

}
