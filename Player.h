#pragma once
#include "Unit.h"

const int MAXNAME = 20;



typedef struct Player{
    char name[MAXNAME];
    int money;
    Unit* units;
    int unit_count;
    int playerNumber;
} Player;


void player_init(Player* player);

Unit* add_unit(int type, Player* player);
void remove_unit(Player* player);
