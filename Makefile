#https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
#$^ contient les variables du dessus

all: game

game: main.o Game.o Player.o Unit.o Map.o display.o
	gcc $^ -o game -lstdc++ -lSDL -lSDL_image -lSDL_ttf -g
	@echo Compilation finie.

clean: 
	rm -f game *.o

%.o: %.cpp %.h
	gcc -c -Wall -lstdc++ -lSDL -lSDL_image -lSDL_ttf -g $<




