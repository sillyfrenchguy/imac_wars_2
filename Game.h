#pragma once
#include "Player.h"
#include "Map.h"
#include "display.h"
#include "SDL/SDL.h"

const int NB_PLAYERS = 2;

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int FPS = 60;
const int frameDelay = 1000 / FPS; //Durée d'une frame
const int margeleft = SCREEN_WIDTH/40;
const int margeright = (SCREEN_WIDTH+MAP_WIDTH*LARGEUR_TILE)/2 + margeleft;
const int margebottom = SCREEN_HEIGHT*(1-1/20);
const int margetop = SCREEN_HEIGHT/20;





typedef struct Game {
    Player players[NB_PLAYERS];
    Map map;

} Game ;



void game_init(Game* game);
void game_start(Game* game);
void game_start2(Game* game);
void game_duel(Game* game);
void gameHandleEvent(Game* game);
void gameClean();
