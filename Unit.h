#ifndef UNIT_H
#define UNIT_H
typedef struct Player Player;


const int NB_UNIT = 3;
const int NB_ACTION = 2;


typedef struct Unit {
    Player* owner;
    int cost; 
    float power; 
    int move;
    float pv;
    int range;
    int super;

} Unit;


extern const char* const UNITS_NAME[NB_UNIT];
extern const char* const UNITS_ACTION[NB_ACTION];
extern const int UNITS_PRICE[NB_UNIT] ;

void soldat_init(Unit* soldat, Player* player);

void bazooka_init(Unit* bazooka, Player* player);

void tank_init(Unit* tank, Player* player);

#endif