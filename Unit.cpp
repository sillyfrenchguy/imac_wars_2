#include "Unit.h"
#include <string.h>
#include <stdio.h>

const char* const UNITS_NAME[NB_UNIT] = {"SOLDAT", "BAZOOKA", "TANK"};
const int UNITS_PRICE[NB_UNIT] = {1000, 2000, 3000};
const char* const UNITS_ACTION[NB_ACTION]= {"SE DEPLACER", "ATTAQUER"};

void soldat_init(Unit* soldat, Player* player){
    soldat->owner = player ;
    soldat->cost = 1000;
    soldat->power = 0.2;
    soldat->move = 6;
    soldat->pv = 100;
    soldat->range = 1;
    soldat->super = 2;
}

void bazooka_init(Unit* bazooka, Player* player){
    bazooka->owner = player ;
    bazooka->cost = 2000;
    bazooka->power = 0.35;
    bazooka->move = 3;
    bazooka->pv = 100;
    bazooka->range = 3;
    bazooka->super = 4;
}

void tank_init(Unit* tank, Player* player){
    tank->owner = player;
    tank->cost = 3000;
    tank->power = 0.5;
    tank->move = 2;
    tank->pv = 100.0;
    tank->range = 6;
    tank->super = 0;
}

