#pragma once
#include"Unit.h"
#include"Player.h"
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include "SDL_image.h"

typedef struct Game Game;

const int MAP_HEIGHT = 15;
const int MAP_WIDTH = 15;

#define LARGEUR_TILE 32
#define HAUTEUR_TILE 32



typedef struct Map {
    int height ;
    int width ;
    Unit* cells[MAP_HEIGHT][MAP_WIDTH];

} Map;

void map_init(Map *map);
void map_draw(Map *map, Game* game);
void Afficher_map(SDL_Surface* screen,SDL_Surface* tileset,char** map,int MAP_WIDTH,int MAP_HEIGHT);
void Afficher_unit(SDL_Surface* screen,SDL_Surface* tileset,Map* map,int MAP_WIDTH,int MAP_HEIGHT);

void add_unit_map(Unit* unit, Map* map, int x, int y);
void remove_unit_map(Map* map, int x, int y);
void info_unit_map(Map* map);
void move_unit_map(Unit* unit, Map* map, int x1, int y1, int x2, int y2);


