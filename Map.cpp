#include <iostream>
#include <bits/stdc++.h>
using namespace std;
#include "Map.h"
#include "Game.h"



void map_init(Map *map) {
    map->width = MAP_WIDTH;
    map->height = MAP_HEIGHT;
    for (int y=0; y<map->height; y++){
        for (int x=0; x<map->width; x++){
            map->cells[y][x] = NULL;
        }
    }

}

void Afficher_map(SDL_Surface* screen,SDL_Surface* tileset,char** map,int MAP_WIDTH,int MAP_HEIGHT)
{
	int i,j;
	SDL_Rect Rect_dest;
	SDL_Rect Rect_source;
	Rect_source.w = LARGEUR_TILE;
	Rect_source.h = HAUTEUR_TILE;
	for(i=0;i<MAP_WIDTH;i++)
	{
		for(j=0;j<MAP_HEIGHT;j++)
		{
			Rect_dest.x = i*LARGEUR_TILE;
			Rect_dest.y = j*HAUTEUR_TILE;
            Rect_source.x = (map[j][i]-'0')*LARGEUR_TILE;
            Rect_source.y = 0;
            SDL_BlitSurface(tileset,&Rect_source,screen,&Rect_dest);


		}
	}
	SDL_Flip(screen);
}

void Afficher_unit(SDL_Surface* screen,SDL_Surface* tileset,Map* map,int MAP_WIDTH,int MAP_HEIGHT)
{

	int i,j;
	SDL_Rect Rect_dest;
	SDL_Rect Rect_source;
	Rect_source.w = LARGEUR_TILE;
	Rect_source.h = HAUTEUR_TILE;
    
	for(i=0;i<MAP_WIDTH;i++)
	{
		for(j=0;j<MAP_HEIGHT;j++)
		{
            
			Rect_dest.x = i*LARGEUR_TILE;
			Rect_dest.y = j*HAUTEUR_TILE;
            
			if(map->cells[j][i] != NULL){
                cout << "Super : " << map->cells[j][i]->super << endl;
                cout << "owner : " << map->cells[j][i]->owner << endl;
                Rect_source.x = (map->cells[j][i]->super + map->cells[j][i]->owner->playerNumber)*LARGEUR_TILE;
                Rect_source.y = 0;

                SDL_BlitSurface(tileset,&Rect_source,screen,&Rect_dest);
                cout << " i = " << i << " j = "<< j << endl;
			}
		}
	}
	SDL_Flip(screen);
}


void map_draw(Map *map, Game* game) {


    /*Ligne 1 : deco */

    cout << "╔═══";
    for (int i=0; i< map->width; i++) {
        cout << "══╦═══";
    }
    cout << "══╗" << endl;

    /*Ligne 2 : chiffre */

    cout << "║   ";
    for (int i=0; i< map->width; i++) {
        cout << "  ║  " << i ;
    }
    cout << "  ║" << endl;

    /*Ligne 3 : deco */

    cout << "╠═══";
    for (int i=0; i< map->width; i++) {
        cout << "══╬══" << "═" ;
    }
    cout << "══╣" << endl;

    /* Contenu : deco + chiffres */

    for (int j=0; j< map->height; j++) {
        //chiffre
        cout << "║  " << j;
        for (int i=0; i< map->width; i++) {
            //quand on a une unité
            if (map->cells[j][i]){
                cout << "  ║ ";

                if ((*map->cells[j][i]).owner == &game->players[0]){
                    cout << "♦";
                }
                if ((*map->cells[j][i]).owner == &game->players[1]){
                    cout << "♠";
                }

                if ((*map->cells[j][i]).super == 1){
                    cout << "S";
                }
                if ((*map->cells[j][i]).super == 2){
                    cout << "B";
                }
                if ((*map->cells[j][i]).super == 3){
                    cout << "T";
                }

            }
            else {
                cout << "  ║   " ;
            }


        }
        cout <<"  ║" << endl;

        //deco
        if (j < map->height - 1) {
            cout << "╠═══";
            for (int i=0; i< map->width; i++) {
                cout << "══╬═══" ;
            }
            cout << "══╣" << endl;

        }
    }

    /*Dernière ligne : deco */

    cout << "╚═══";
    for (int i=0; i< map->width; i++) {
        cout << "══╩═══";
    }
    cout << "══╝" << endl;

}

void add_unit_map(Unit* unit, Map* map, int x, int y){
    map->cells[y][x] = unit;
}

void remove_unit_map(Map* map, int x, int y){
    map->cells[y][x] = NULL;
}

void info_unit_map(Map* map){
    for (int j=0; j< map->height; j++){
        for (int i=0; i<map->width; i++){
                if (map->cells[j][i] != NULL) {
                    cout << "(" << i << ";" << j << ") ["<<(*(map->cells[j][i])->owner).name <<"]    Type : "<< UNITS_NAME[(*map->cells[j][i]).super - 1] <<"    Pv :" << setprecision(3) << (*map->cells[j][i]).pv << "/100" << endl ;
                }
        }
    }
}

void move_unit_map(Unit* unit, Map* map, int x1, int y1, int x2, int y2){
    map->cells[y2][x2] = unit;
    map->cells[y1][x1] = NULL; //l'ancienne case se vide
}
